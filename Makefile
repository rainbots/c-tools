file-in-Vim-window: build/${file-stem}.exe
#file-in-Vim-window: build/${file-stem}.md

CFLAGS = -g -Wall -Wextra -pedantic

build/%.md: build/%.exe
	./$^ > $@

build/%.exe: src/%.c
	${compiler} $^ -o $@ ${CFLAGS}

clean-all-builds:
	rm -f build/hello-frame-stack.*
#	rm -f build/hello-sum.*
#	rm -f build/hello-int.*
