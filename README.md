# Table of Contents
- [Invoking](#markdown-header-invoking)
    - [Vim shortcut](#markdown-header-vim-shortcut)
    - [Make from the terminal](#markdown-header-make-from-the-terminal)
- [Viewing](#markdown-header-viewing)
- [Links to c-tools docs](#markdown-header-links-to-c-tools-docs)

---e-n-d---
# Invoking
---
Invoke by Vim shortcut or by manually entering the `make` command at the
terminal.

## Vim shortcut
---
`{file-stem}` is passed in by:

    ;mrc / ;mrk / ;mr+

These commands:

* build the markdown from the .c source
* then open the markdown in a Vim window.

`;r<Space>` closes the markdown window (invoked from `.c` or `.md` window).

The last letter indicates the compiler:

- `c` - gcc
- `l` - clang
- `+` - g++

Alternatively, `{file-stem}` is passed in by:

    ;mkc / ;mkl / ;mk+

These commands just build the markdown. Invoke `;re` to open the markdown in a Vim window (invoked from the `.c` window, and the NERDTree must be at the project root where the `build` folder is visible).

## Make from the terminal
---
Alternatively, manually invoke make from the terminal. At the Vim command line:
```vim
:!make compiler=clang file-stem=%:t:r
```
Or `:terminal` (shortcut `<F2>`) and enter the same:
```bash
:!make compiler=clang file-stem=%:t:r
```

- `%` is the current Vim file
- `t` removes the path
- `r` removes the extension

Invoke `make` with the `-n` flag to preview targets/recipes with arguments `compiler` and `file-stem` expanded.

# Viewing
---
Instead of the Vim window (`;re` and `;r<Space>`), view the markdown with *Markdown Viewer*.

Open the markdown with `;re`. From the markdown window:
```vim
;pp
"+P
;cw
```
`;pp` gets the markdown file path with:
```vim
:let @+ = expand("%:p")
```
`"+P` pastes from the clipboard and leaves the cursor on the path text. `;cw`
invokes `cypgath -w` to convert the POSIX path to a Windows path. The Windows
path is now in the clipboard. From a PowerShell terminal:
```powershell
chrome "{paste_path_from_clipboard}"
```
Then just Alt-Tab to Chrome and `F5` to refresh each time the markdown is
built.

# Links to c-tools docs
- Clone [this repository][this-repo] (note `git clone` link is not the same as
  `this repository` link):
[this-repo]: https://bitbucket.org/rainbots/c-tools/src/master/

    git clone https://rainbots@bitbucket.org/rainbots/c-tools.git

- A [short example][hello-frame-stack] to look at the *frame stack* in *gdb*.
[hello-frame-stack]: https://bitbucket.org/rainbots/c-tools/src/master/src/hello-frame-stack.c


