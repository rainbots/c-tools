#include <stdio.h>
#include <assert.h>

int WhatNumberOddNumberIs(int odd_number) { return (odd_number+1)/2; }
int SumOfOddsFrom1To(int odd_number)
{
    int n = WhatNumberOddNumberIs(odd_number);
    return n*n;
}

int main() {
    printf("%d", SumOfOddsFrom1To(19));
    assert(100 == SumOfOddsFrom1To(19));
}
